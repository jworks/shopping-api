# Shopping Api (using Play Scala Starter)
This is a shopping api that allows you to create new users, new products, new shopping cards, fill them of products and create buy orders. 

## Running

Run this using [sbt](http://www.scala-sbt.org/).
```
sbt run
```

And then go to http://localhost:9000 to see available api endpoints

## Controllers

- HomeController.scala:

  handle all HTTP requests.


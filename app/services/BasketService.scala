package services

import java.util.UUID

import models.{Basket, Product}


class BasketService (userService: UserService, productService: ProductService, orderService: OrderService)
{

  var basketMap: Map[String, Basket] = Map()

  def createBasket(userEmail: String): Boolean = {

    if(userService.userExists(userEmail) && !basketExists(userEmail)){
      basketMap += (userEmail -> new Basket(userEmail, Map()))
      true
    }else{
      false
    }
  }

  def basketExists(userEmail: String): Boolean = {

    if((basketMap isDefinedAt(userEmail)))
      true
    else
      false
  }

  def insertItem(userEmail: String, productId: Int): Boolean = {
    if(!basketExists(userEmail))
      createBasket(userEmail)

    if(productService.productExist(productId) && productService.getStock(productId) > 0){

      if(isProductInBasket(userEmail, productId)){
        increaseProductQuantity(userEmail, productId)
      }else{
        insertProductInBasket(userEmail, productId)
      }
      productService.decreaseStock(productId)

      true
    }else{
      false
    }
  }

  def isProductInBasket(email: String, id: Int): Boolean = {
    (basketMap get email).head.productMap isDefinedAt(id)
  }

  def insertProductInBasket(userEmail: String, productId: Int): Unit = {
    val price       = productService.getPrice(productId)
    val description = productService.getDescription(productId)
    val stock       = 1
    val product     = new Product(productId, price, description, stock)
    (basketMap get userEmail).head.productMap += (productId -> product)
  }

  def increaseProductQuantity(userEmail: String, productId: Int): Unit = {
    var currentStock = ((basketMap get userEmail).head.productMap get productId).head.stock
    currentStock = currentStock + 1
    ((basketMap get userEmail).head.productMap get productId).head.stock = currentStock
  }

  def isBasketEmpty(userEmail: String): Boolean = {
    basketMap.isEmpty
  }

  def getBasket(userEmail: String): Basket = {
    (basketMap get userEmail).head
  }

  def checkOut(userEmail: String, address: String): UUID = {
    if(userService.userExists(userEmail) && basketExists(userEmail) && !isBasketEmpty(userEmail)){
      val uuid = orderService.createOrder(userEmail, address, getBasket(userEmail))
      uuid
    }else{
      null
    }
  }
}

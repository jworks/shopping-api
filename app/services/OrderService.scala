package services

import java.util.UUID

import models.{Basket, Order}

class OrderService()
{
  var orderMap: Map[UUID, Order] = Map()

  def createOrder(mail: String, address: String, basket: Basket): UUID = {
    val uuid = UUID.randomUUID()
    orderMap += (uuid -> new Order(uuid, mail, address, basket))
    uuid
  }

}

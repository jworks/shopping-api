package services

import models.User
import scala.collection.mutable.ListBuffer

class UserService {

  var userMap: Map[String, User] = Map()

  def addUser(name: String, password: String, bankAccount: String, email: String): Boolean = {

    if(!(userMap isDefinedAt(email))){
      userMap = userMap + (email -> new User(name, password, bankAccount, email))
      true
    }else{
      false
    }
  }

  def getUser(email: String): Option[User] = {
    userMap get email
  }

  def userExists(email: String): Boolean = {
    if(userMap isDefinedAt(email))
      true
    else
      false
  }

}

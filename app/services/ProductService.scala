package services

import models.Product

class ProductService {

  var productMap: Map[Int, Product] = Map()
  var idCounter = 1

  def addProduct(price: Double, description: String): Int = {
    val id = productMap.size + 1
    productMap = productMap + (id -> new Product(idCounter, price, description, 0))
    increaseIdCounter()
    id
  }

  def increaseIdCounter(): Unit = {
    idCounter = idCounter + 1
  }

  def getProduct(id: Int): Product = {
    productMap get id match {
      case Some(product) => product
    }
  }

  def productExist(id: Int): Boolean = {
    if(productMap isDefinedAt(id))
      true
    else
      false
  }

  def getStock(id: Int): Int = {
    (productMap get id) match {
      case Some(product) => product.stock
      case None          => 0
    }
  }

  def increaseStock(id: Int): Unit = {
    if (productExist(id)){
      val currentStock = (productMap get id).head.stock
      (productMap get id).head.stock = currentStock + 1
    }else {
      false
    }
  }

  def decreaseStock(id: Int): Unit = {
    if (productExist(id) ){
      val targetProduct = (productMap get id).head
      if(targetProduct.stock > 0){
        (productMap get id).head.stock = targetProduct.stock - 1
      }
    }
  }

  def getPrice(id: Int): Double = {
    (productMap get id).head.price
  }

  def getDescription(id: Int): String = {
    (productMap get id).head.description
  }

}

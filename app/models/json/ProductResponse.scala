package models.json

import play.api.libs.json.{JsValue, Json}

class ProductResponse(id: Int, val message: String) {
  val json: JsValue = Json.parse(
    """{
       "response": """" + message + """",
       "id": """+id+"""
    }
    """.stripMargin)
}

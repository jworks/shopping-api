package models.json

import play.api.libs.json.{JsValue, Json}

class PostMessage(val message: String) {
  val json: JsValue = Json.parse(
    """{
       "response": """" + message + """"
    }
    """.stripMargin)
}

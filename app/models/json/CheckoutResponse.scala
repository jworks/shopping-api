package models.json

import java.util.UUID
import play.api.libs.json.{JsValue, Json}

class CheckoutResponse(uuid: UUID, message: String) {
  val json: JsValue = Json.parse(
    """{
       "response": """" + message + """",
       "uuid":"""" + uuid +""""
    }
    """.stripMargin)
}

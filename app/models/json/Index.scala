package models.json
import play.api.libs.json._

class Index {
  val json: JsValue = Json.parse(
    """{
        |"available_endpoints": [
        | {"add_user": "localhost:9000/add_user"},
        | {"add_product": "localhost:9000/add_product"},
        | {"increase_stock": "localhost:9000/increase_stock"},
        | {"create_basket": "localhost:9000/create_basket"},
        | {"insert_item": "localhost:9000/insert_item"},
        | {"checkout": "localhost:9000/checkout"}
        |]
      |}
    """.stripMargin)
}

package models

import java.util.UUID

class Order(var id: UUID, var userEmail: String, var address: String, var basket: Basket)

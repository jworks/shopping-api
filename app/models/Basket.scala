package models

import scala.collection.mutable.ListBuffer

class Basket(var userEmail: String, var productMap: Map[Int, Product])
{

  override def toString: String = "mail: " + userEmail + " products: " + productMap
}

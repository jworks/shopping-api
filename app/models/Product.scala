package models

class Product(var id: Int, var price: Double, var description: String, var stock: Int)

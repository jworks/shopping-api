package controllers

import javax.inject._

import play.api._
import play.api.mvc._
import play.api.libs.json._
import services.{BasketService, OrderService, ProductService, UserService}
import models.json.{CheckoutResponse, Index, PostMessage, ProductResponse}
import play.api.libs.json.Reads._
import play.api.libs.functional.syntax._


case class UserVal(name: String, password: String, bankAccount: String, email: String)
case class ProductVal(price: Double, description: String)
case class BasketVal(email: String)
case class InsertProductVal(email:String, id: Int)
case class StockVal(id: Int)
case class CheckoutVal(email: String, address: String)

/**
 * This controller creates an `Action` to handle HTTP requests to the
 * application's home page.
 */
@Singleton
class HomeController @Inject()
(userService: UserService, productService: ProductService, orderService: OrderService)
  extends Controller {

  var basketService = new BasketService(userService, productService, orderService)

  implicit val usrReads: Reads[UserVal] = (
      (JsPath \ "name").read[String] and
      (JsPath \ "password").read[String] and
      (JsPath \ "bankAccount").read[String] and
      (JsPath \ "email").read[String]
    )(UserVal.apply _)

  implicit val productReads: Reads[ProductVal] = (
      (JsPath \ "price").read[Double] and
      (JsPath \ "description").read[String]
    )(ProductVal.apply _)

  implicit val insertProductReads: Reads[InsertProductVal] = (
      (JsPath \ "email").read[String] and
      (JsPath \ "id").read[Int]
    )(InsertProductVal.apply _)

  implicit val basketReads: Reads[BasketVal] = {
    ((__ \ "email").read[String]).map(BasketVal(_))
  }

  implicit val stockReads: Reads[StockVal] = {
    ((__ \ "id").read[Int]).map(StockVal(_))
  }

  implicit val checkoutReads: Reads[CheckoutVal] = (
      (JsPath \ "email").read[String] and
      (JsPath \ "address").read[String]
    )(CheckoutVal.apply _)


  def index = Action {
    Ok(new Index().json)
  }

  def addUser = Action(parse.json) { request =>
    val placeResult = request.body.validate[UserVal]
    placeResult.fold(
      errors => {
        BadRequest(new PostMessage("bad_request").json)
      },
      user => {
        userService.addUser(user.name, user.password, user.bankAccount, user.email)
        Ok(new PostMessage("user_created").json)
      }
    )
  }

  def addProduct = Action(parse.json) { request =>
    val placeResult = request.body.validate[ProductVal]
    placeResult.fold(
      errors => {
        BadRequest(new PostMessage("bad_request").json)
      },
      product => {
        val id = productService.addProduct(product.price, product.description)
        Ok(new ProductResponse(id, "product_created").json)
      }
    )
  }

  def createBasket = Action(parse.json) { request =>
    val placeResult = request.body.validate[BasketVal]
    placeResult.fold(
      errors => {
        BadRequest(new PostMessage("bad_request").json)
      },
      basket => {
        val result = basketService.createBasket(basket.email)

        if(result){
          Ok(new PostMessage("basket_created").json)
        }else{
          Conflict(new PostMessage("could_not_create_basket").json)
        }
      }
    )
  }

  def insertProductInBasket = Action(parse.json) { request =>
    val placeResult = request.body.validate[InsertProductVal]
    placeResult.fold(
      errors => {
        BadRequest(new PostMessage("bad_request").json)
      },
      product => {
        if(basketService.insertItem(product.email, product.id)){
          Ok(new PostMessage("product_inserted_in_basket").json)
        }else{
          Conflict(new PostMessage("could_not_insert_product_in_basket").json)
        }
      }
    )
  }

  def increaseProductStock = Action(parse.json) { request =>
    val placeResult = request.body.validate[StockVal]
    placeResult.fold(
      errors => {
        BadRequest(new PostMessage("bad_request").json)
      },
      stock => {
        if(productService.productExist(stock.id)){
          productService.increaseStock(stock.id)
          Ok(new PostMessage("increased_product_stock").json)
        }else{
          Conflict(new PostMessage("could_not_increase_product_stock").json)
        }
      }
    )
  }


  def checkout = Action(parse.json) { request =>
    val placeResult = request.body.validate[CheckoutVal]
    placeResult.fold(
      errors => {
        BadRequest(new PostMessage("bad_request").json)
      },
      checkout => {
        val uuid = basketService.checkOut(checkout.email, checkout.address)
        if( uuid != null){
          Ok(new CheckoutResponse(uuid, "order_created").json)
        }else{
          Conflict(new PostMessage("could_not_generate_order").json)
        }
      }
    )
  }

}
